import logging
import os
import re
import mammoth
import mammoth.transforms
import mondatbank.helpers as hlp
from mondatbank.fejezet import *
from bs4 import BeautifulSoup
from mondatbank.db import global_init
from mondatbank.models import *
from datetime import datetime

from cliff.show import ShowOne


class File(ShowOne):
    "Parsing a Natura2000 document"

    log = logging.getLogger(__name__)
    sections = {}
    data = {}
    metrics = { 
            "terulet_neve": "",
            "terulet_azonositoja": "",
            "fejezetszam" : 0,
            "azonosito_adatok_fejezet": 0,
            "teruleti_celkituzesek_fejezet": 0,
            "fajok_szama_adatlap": 0,
            "faj_fejezetek_szama": 0,
            "elohelyek_szama_adatlap": 0,
            "elohely_fejezetek_szama": 0,
            "teruleti_celkituzesek_lista": 0,
            "ismeretlen_fejezetek_szama": 0
            }

    def get_parser(self, prog_name):
        parser = super(File, self).get_parser(prog_name)
        parser.add_argument('filename', nargs='?', default='.')
        parser.add_argument('--save-html', action='store_true', help='save the doc as a html file')
        parser.add_argument('--save-json', action='store_true', help='save the parsed document as a JSON file')
        return parser

    def take_action(self, parsed_args):
        """The main function which run on call the parse command"""

        global_init()

        # Dokumentum beolvasása
        body = self.read_document(parsed_args.filename, save_html = parsed_args.save_html)
        self.log.debug('Dokumentum beolvasása sikeres!\n-------------------------------')

        # Fejezetek azonosítása
        headings = self.get_headings(body)
        self.log.debug('Azonosított fejezetek száma: ' + str(len(headings)) + '\n-------------------------------')

        # Fejezetek beolvasása
        self.build_section_objects(body)
        self.log.debug("Dokumentum fejezeteinek beolvasása sikeres!\n-------------------------------")

        # Statisztika számítasa
        self.count_objects()

        # Részletes értelmezés és adatbázis mentés
        self.save_db(save_json = parsed_args.save_json)
        
        columns = (
                'Terület neve',
                'Terület azonositoja',
                'Fejezetek száma',
                'Azonosito adatok fejezet',
                'Területi célkitűzések fejezet',
                'Jelölő fajok száma az azonsító adatokban', 
                'Jelölő fajok azonosított fejezetek',
                'Jelölő élőhelyek száma az azonsító adatokban', 
                'Jelölő élőhelyek azonosított fejezetek',
                'Területi célkitűzések',
                'Azonosítatlan típusú fejezetek száma'
                )
        data = tuple(self.metrics.values())
        return (columns, data)


    def read_document(self, filename, save_html = False):
        """ Reads the document, converts it to html and returns the document body as a BeautifulSoup object"""
        f = open(filename, 'rb')
        transform_document = mammoth.transforms.paragraph(hlp.transform_paragraph)
        document = mammoth.convert_to_html(f, transform_document=transform_document)
        f.close()

        if save_html == True:
            fn = filename.split('/')
            fn = fn[-1].rsplit( ".", 1 )[ 0 ]
            fn = 'html/' + fn + '.html'

            os.makedirs('html', exist_ok = True)
            o = open(fn, 'w')
            o.write(document.value)
            o.close()
            self.log.debug(fn + ' fájl mentve')

        soup = BeautifulSoup(document.value, 'lxml')
        
        # Removing unnecessary elements (images, empty paragraphs)
        for img in soup.body.find_all('img'):
            img.extract()
        for a in soup.body.find_all(True):
            if len(a.text) == 0:
                a.extract()
        
        return soup.body


    def build_section_objects(self, body):
        self.log.debug('Fejezetek értelmezése:')
        # Datum
        self.data['datum'] = hlp.find_date_before_h2(body)
        if self.data['datum'] == None:
            raise Exception('A dátumot nem sikerült azonosítani')

        sections = body.find_all('h2')
        azonosito_adatok_exists = False
        teruleti_celkituzesek_exists = False
        for section_idx, sec in enumerate(sections):
            # general section object to determine the section type
            fejezet = Fejezet([sec])

            self.log.debug('    ' + str(section_idx + 1) + '. ' + fejezet.name)
            
            if fejezet.get_type() == 'AzonositoAdatok':
                azonosito_adatok_exists = True
            if fejezet.get_type() == 'TeruletiCelkituzesek':
                teruleti_celkituzesek_exists = True

            if section_idx == 0 and not azonosito_adatok_exists:
                raise Exception('\'Azonosító adatok\' fejezet hiányzik, vagy hibás.')
            if section_idx == 1 and not teruleti_celkituzesek_exists:
                raise Exception('\'Területi szintű (átfogó) célkitűzések\' fejezet hiányzik, vagy hibás.')
            if fejezet.get_type() == 'ismeretlen':
                raise Exception('azonosíthatatlan fejezet: ' + fejezet.name)

            # appending the section paragraphs to the object
            for sbl in sec.next_siblings:
                fejezet.append(sbl)
                if sbl.next_sibling and sbl.next_sibling.name == 'h2':
                    break

            self.sections[fejezet.slug] = fejezet
            self.data[fejezet.slug] = fejezet.extract_data()
        
        return self.data
        

    def count_objects(self):
        self.metrics["terulet_neve"] = self.data['azonosito_adatok']['terulet_neve']
        self.metrics["terulet_azonositoja"] = self.data['azonosito_adatok']['terulet_azonositoja']
        self.metrics["fejezetszam"] = len(self.data)
        self.metrics["azonosito_adatok_fejezet"] = int('azonosito_adatok' in self.data.keys())
        self.metrics["teruleti_celkituzesek_fejezet"] = int('teruleti_szintu_atfogo_celkituzesek' in self.data.keys())

        if ('jelolo_madarfajok' in self.data['azonosito_adatok'].keys()):
            self.metrics["fajok_szama_adatlap"] = len(self.data['azonosito_adatok']['jelolo_madarfajok'])
        elif ('jelolo_fajok' in self.data['azonosito_adatok'].keys()):
            self.metrics["fajok_szama_adatlap"] = len(self.data['azonosito_adatok']['jelolo_fajok'])
        
        if ('jelolo_elohelyek' in self.data['azonosito_adatok'].keys()):
            self.metrics["elohelyek_szama_adatlap"] = len(self.data['azonosito_adatok']['jelolo_elohelyek'])

        for fejezet in self.sections.values():
            if (fejezet.get_type() == 'FajAdatlap'):
                self.metrics['faj_fejezetek_szama'] += 1
            if (fejezet.get_type() == 'ElohelyAdatlap'):
                self.metrics['elohely_fejezetek_szama'] += 1
            if (fejezet.get_type() == 'ismeretlen'):
                self.metrics['ismeretlen_fejezetek_szama'] += 1
                self.log.debug('Nem sikerult azonosítani: ' + fejezet.name)
        

    def get_headings(self, body):
        """Retruns the identified headings with the markup stripped out"""
        return [hlp.remove_markup(h) for h in body.find_all('h2')]


    def save_db(self, save_json = False):
        if (not 'azonosito_adatok' in self.data.keys()):
            raise Exception('azonosito_adatok hiányzik a self.data-ból')
        
        try:
            site = Site.objects(terulet_azonositoja=self.data['azonosito_adatok']['terulet_azonositoja']).get()
        except:
            site = Site()
            site.terulet_azonositoja = self.data['azonosito_adatok']['terulet_azonositoja']
            site.terulet_neve = self.data['azonosito_adatok']['terulet_neve']
            site.terulet_tipusa = self.data['azonosito_adatok']['terulet_tipusa']
            site.terulet_kiterjedese = hlp.string_to_float(self.data['azonosito_adatok']['terulet_kiterjedese'])
            site.updated_at = datetime.now()
            site.created_at = datetime.now()
            site.save()

        doc = SiteDocument()
        doc.site_id = str(site.id)
        doc.datum = self.data['datum']
        if 'felhasznalt_natura_2000_fenntartasi_terv_keszultsege_elerhetosege' in self.data['azonosito_adatok']:
            self.data['azonosito_adatok']['natura_2000_fenntartasi_terv_keszultsege_elerhetosege'] = self.data['azonosito_adatok']['felhasznalt_natura_2000_fenntartasi_terv_keszultsege_elerhetosege']
        doc.keszultseg = self.parse_keszultseg(self.data['azonosito_adatok']['natura_2000_fenntartasi_terv_keszultsege_elerhetosege'])

        if 'jelolo_madarfajok' in self.data['azonosito_adatok']:
            self.data['azonosito_adatok']['jelolo_fajok'] = self.data['azonosito_adatok']['jelolo_madarfajok']
        
        if 'jelolo_fajok' in self.data['azonosito_adatok']:
            self.log.debug('Jelölő fajok adatainak értelmezése:')
            doc.jelolo_fajok = []
            for j in self.data['azonosito_adatok']['jelolo_fajok']:
                self.log.debug('    - ' + j)
                faj = self.prep_faj(j)
                doc.jelolo_fajok.append(faj)
            self.log.debug('\n-------------------------------')
        
        if 'jelolo_elohelyek' in self.data['azonosito_adatok']:
            self.log.debug('Jelölő élőhelyek adatainak értelmezése:')
            doc.jelolo_elohelyek = []
            for j in self.data['azonosito_adatok']['jelolo_elohelyek']:
                self.log.debug('    - ' + j)
                elohely = self.prep_elohely(j)
                doc.jelolo_elohelyek.append(elohely)
            self.log.debug('\n-------------------------------')
        
        if 'teruleti_szintu_atfogo_celkituzesek' in self.data:
            for ck in self.data['teruleti_szintu_atfogo_celkituzesek']:
                if isinstance(ck, str):
                    doc.teruleti_celkituzesek.append(ck)
                elif isinstance(ck, dict):
                    for pri in ck['children']:
                        doc.teruleti_celkituzesek.append(ck['value'] + ' ' + pri)
        
        if (save_json):
            os.makedirs('json', exist_ok = True)
            with open('json/{} - {}.json'.format(site.terulet_azonositoja, site.terulet_neve), 'w') as outfile:
                json.dump(doc.to_json(), outfile)

        doc.save()

    def prep_elohely(self, name):
        slug = hlp.slugify(name)
        if not slug in self.data.keys():
            raise Exception('Nem talalálható \'' + name + '\' fejezet a dokumentumban!')
        
        missing_attributes = [value for value in ['kiterjedes', 'reprezentativitas', 'trend', 'termeszetvedelmi_helyzet', 'prioritas', 'az_elohelytipushoz_kotodo_jelolo_fajok', 'celkituzesek'] if value not in self.data[slug].keys()]
        if (len(missing_attributes)):
            raise Exception('Hiányzó vagy hibás attribútumok: ' + ', '.join(missing_attributes))

        elohely = Elohely()
        elohely.elohely = name
        elohely.kiterjedes = self.parse_kiterjedes(self.data[slug]['kiterjedes'])
        elohely.reprezentativitas = self.parse_populacio(self.data[slug]['reprezentativitas'])
        elohely.trend = self.data[slug]['trend']
        elohely.termeszetvedelmi_helyzet = self.parse_termeszetvedelmi_helyzet(self.data[slug]['termeszetvedelmi_helyzet'])
        elohely.prioritas = self.parse_prioritas(self.data[slug]['prioritas'])
        elohely.kotodo_fajok = []
        for faj in self.data[slug]['az_elohelytipushoz_kotodo_jelolo_fajok']:
            elohely.kotodo_fajok.append(faj)

        if 'celkituzesek' in self.data[slug]:
            elohely.celkituzesek = []
            for celkituzes in self.data[slug]['celkituzesek']:
                ckt = Celkituzes()
                for k in celkituzes.keys():
                    setattr(ckt, k, celkituzes[k])

                elohely.celkituzesek.append(ckt)

        return elohely

    def prep_faj(self, name):
        slug = hlp.slugify(name)
        if not slug in self.data.keys():
            raise Exception('Nem talalálható \'' + name + '\' fejezet a dokumentumban!')
        
        if 'a_faj_elofordulasaval_erintett_kozossegi_jelentosegu_elohelytipusok' in self.data[slug]:
            self.data[slug]['a_faj_elofordulasaval_erintett_jelolo_elohelytipusok'] = self.data[slug]['a_faj_elofordulasaval_erintett_kozossegi_jelentosegu_elohelytipusok']

        missing_attributes = [value for value in ['allomanynagysag', 'populacio', 'trend', 'termeszetvedelmi_helyzet', 'prioritas', 'a_faj_elofordulasaval_erintett_jelolo_elohelytipusok', 'celkituzesek'] if value not in self.data[slug].keys()]
        if (len(missing_attributes)):
            raise Exception('Hiányzó vagy hibás attribútumok: ' + ', '.join(missing_attributes))

        faj = Faj()
        faj.faj = self.data[slug]['magyar_nev'] + ' ('+self.data[slug]['latin_nev']+')'
        faj.allomany_tipus = self.data[slug]['allomany_tipus']
        faj.jelolonek_javasolt = self.data[slug]['jelolonek_javasolt']
        faj.allomanynagysag = self.parse_allomanynagysag(self.data[slug]['allomanynagysag'])
        faj.populacio = self.parse_populacio(self.data[slug]['populacio'])
        faj.trend = self.data[slug]['trend']
        faj.termeszetvedelmi_helyzet = self.parse_termeszetvedelmi_helyzet(self.data[slug]['termeszetvedelmi_helyzet'])
        faj.prioritas = self.parse_prioritas(self.data[slug]['prioritas'])
        faj.kotodo_elohelyek = []

        
        
        if 'a_faj_elofordulasaval_erintett_jelolo_elohelytipusok' in self.data[slug]:
            for elohely in self.data[slug]['a_faj_elofordulasaval_erintett_jelolo_elohelytipusok']:
                faj.kotodo_elohelyek.append(elohely)

        if 'celkituzesek' in self.data[slug]:
            faj.celkituzesek = []
            for celkituzes in self.data[slug]['celkituzesek']:
                ckt = Celkituzes()
                for k in celkituzes.keys():
                    key = 'eszkozok' if k == 'eszkozok_a_celkituzesek_eleresere' else k
                    setattr(ckt, key, celkituzes[k])

                faj.celkituzesek.append(ckt)
        
        return faj

    def parse_allomanynagysag(self, allomanynagysag):
        pattern = r'(?P<min>\d+)-(?P<max>\d+) (?P<unit>\w+) \((SDF (?P<sdf>\d+))\),?\s*(?P<comments>.*)|(?P<value>.+) \((SDF (?P<sdf2>\d+))\),?\s*(?P<comments2>.*)'
        match = re.match(pattern, allomanynagysag)
        an = Allomanynagysag()
        an.exact_values = False

        if match:
            an.min = int(match.group("min")) if match.group("min") else None
            an.max = int(match.group("max")) if match.group("max") else None
            an.unit = match.group("unit")
            an.sdf = match.group("sdf") or match.group("sdf2")
            an.comments = match.group("comments") or match.group("comments2") or ""
            an.comments = an.comments.strip()
            an.value = None if an.min and an.max else match.group("value")
            an.exact_values = True if an.min and an.max else False
        
        return an
    
    def parse_kiterjedes(self, kiterjedes):
        pattern = r'(?P<value>[\w,]+) (?P<unit>\w+) \((SDF (?P<sdf>\d+))\),?\s*(?P<comments>.*)'
        match = re.match(pattern, kiterjedes)
        an = Kiterjedes()

        if match:
            an.value = match.group("value").replace(",",".")
            an.unit = match.group("unit")
            an.sdf = match.group("sdf") or match.group("sdf2")
            an.comments = match.group("comments") or ""
            an.comments = an.comments.strip()
        else:
            an.comments = kiterjedes
        
        return an

    def parse_populacio(self, populacio):
        pattern = r'(?P<value>[A-C\-]) \((SDF (?P<sdf>\d+))\),?\s*(?P<comments>.*)'
        match = re.match(pattern, populacio)
        pop = Populacio()

        if match:
            pop.value = match.group("value")
            pop.sdf = match.group("sdf")
            pop.comments = match.group("comments")
        else:
            pop.comments = populacio
        
        return pop

    def parse_termeszetvedelmi_helyzet(self, termeszetvedelmi_helyzet):
        # csak alap parsolas a helyzet komplikáltsága miatt
        th = TermeszetvedelmiHelyzet()
        pattern = r'(?P<value>(kedvező|nem kielégítő|rossz)),?\s?(\((?P<trend>.*)\))?,?\s*?(?P<comments>.*)'
        match = re.match(pattern, termeszetvedelmi_helyzet)
        
        if match:
            th.value = match.group('value')
            th.trend = match.group('trend')
            th.comments = match.group('comments')
        else:
            th.comments = termeszetvedelmi_helyzet

        th.subject = 'általános'
        th.original_string = termeszetvedelmi_helyzet
        return [th]
    
    def parse_prioritas(self, prioritas):
        p = Prioritas()
        pattern = r'(?P<value>(igen|nem))\s?(\((?P<level>(országos szintű|területi szintű))\))?,?\s*?(?P<comments>.*)'
        match = re.match(pattern, prioritas)
        
        if match:
            p.value = match.group('value')
            p.level = match.group('level')
            p.comments = match.group('comments')
        else:
            p.value = ""
            p.comments = prioritas

        return p

    def parse_keszultseg(self, keszultseg):
        pattern = r'(?P<status>\w+(?: \w+)?) ?\(?(?:elfogadás éve: )?(?P<year>\d{4})?\)? ?(?P<url>https?:\/\/[^\s]+)?'
        match = re.match(pattern, keszultseg)

        if match:
            return {
                'status': match.group("status"),
                'year': match.group("year"),
                'url': match.group("url")
            }
        else:
            return {
                'status': keszultseg
            }