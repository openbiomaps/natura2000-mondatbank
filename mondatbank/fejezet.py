import re
import logging
import json_fix
import json
from mondatbank.mondat import *
import mondatbank.helpers as hlp


class Fejezet:

    slug = ''
    elements = []
    tmp = ''
    debug_enabled = False
    log = logging.getLogger(__name__)

    def __init__(self, fejezet):
        self.elements = fejezet
        self.name = fejezet[0].get_text().replace('\t', '').strip()
        self.make_slug()

    def append(self, element):
        self.elements.append(element)

    def make_slug(self):
        self.slug = hlp.slugify(self.name)

    def get_type(self):
        if self.slug == 'azonosito_adatok':
            return 'AzonositoAdatok'
        elif self.slug == 'teruleti_szintu_atfogo_celkituzesek':
            return 'TeruletiCelkituzesek'
        elif re.match(r"^([0-9][0-6][1-9A-Z][0A]\*?).*", self.slug, re.I):
            return 'ElohelyAdatlap'
        elif re.match(
                r"^[\w\- .]+\((?:[\w\- .]+)\)?[\w\- .]*\(?(?:[\w\- .\(\),:]+)?\)?\*?$",
                hlp.slugify(self.name, True),
                re.I
        ):
            return 'FajAdatlap'
        else:
            return 'ismeretlen'

    def debug(self):
        if self.debug_enabled is True:
            print(json.dumps(self.data, indent=2))

    def extract_data(self):
        d = {}
        for i, elem in enumerate(self.elements):
            if i == 0:
                d['fejezet'] = elem.get_text()
                if self.get_type() == 'FajAdatlap':
                    # print(elem.get_text())
                    pattern = r'^([\wáéőúűóüöÁÉŐÚŰÓÜÖ\- .]+) \(([\wáéőúűóüöÁÉŐÚŰÓÜÖ\- .]+)\)?([\wáéőúűóüöÁÉŐÚŰÓÜÖ\- .]*)\(?([\wáéőúűóüöÁÉŐÚŰÓÜÖ\- .\(\),:]+)?\)?\*?$'
                    match = re.match(pattern, elem.get_text(), re.I)
                    grps = list(match.groups())
                    # print(grps)
                    d['magyar_nev'] = grps[0]
                    d['latin_nev'] = grps[1]
                    d['allomany_tipus'] = ''
                    if grps[2].strip() != '' and grps[3] is not None:
                        d['allomany_tipus'] = grps[2].strip() + ' (' + grps[3]
                        grps[3] = None
                    d['szinonima'] = ''
                    d['jelolonek_javasolt'] = False
                    if (grps[3] is not None):
                        four = grps[3][:-1]
                        if four == 'jelölőnek javasolt érték':
                            d['jelolonek_javasolt'] = True
                        else:
                            d['szinonima'] = four

                continue
            
            match = re.match(
                r"^[\w+\b, ?]+:\s+.+$",
                elem.get_text(),
                flags=re.I | re.UNICODE
            )
            if (match):
                m = Egysoros(elem)
                d[m.key] = m.value

            match = re.match(
                r"^[\w+\b ?]+:$",
                elem.get_text().strip(),
                flags=re.I | re.UNICODE
            )

            if (match and (elem.next_sibling.name ==
                           'ul' or elem.next_sibling.name == 'table')):
                self.tmp = elem.get_text().strip()
                continue
            
            if (elem.name == 'ul'):

                if (self.tmp):
                    elem.title = self.tmp
                    del self.tmp
                m = Lista(elem)
                if self.get_type() == 'TeruletiCelkituzesek':
                    if len(self.elements) > 2:
                        raise Exception('\'Területi szintű (átfogó) célkitűzések\' fejezet nem egy listából.')
                    if type(d) is dict:
                        d = []
                    d = d + m.value
                else:
                    d[m.key] = m.value

            if (elem.name == 'table'):
                if (self.tmp):
                    elem.title = self.tmp
                    del self.tmp
                m = Tablazat(elem)
                d[m.key] = m.value
        
        return d
