import mongoengine
from bson.objectid import ObjectId
class Allomanynagysag(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    exact_values = mongoengine.BooleanField(required=True)
    min = mongoengine.IntField()
    max = mongoengine.IntField()
    unit = mongoengine.StringField()
    value = mongoengine.StringField()
    sdf = mongoengine.StringField()
    comments = mongoengine.StringField()

class Kiterjedes(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    value = mongoengine.StringField()
    unit = mongoengine.StringField()
    sdf = mongoengine.StringField()
    comments = mongoengine.StringField()

class Populacio(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    value = mongoengine.StringField()
    sdf = mongoengine.StringField()
    comments = mongoengine.StringField()

class TermeszetvedelmiHelyzet(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    subject = mongoengine.StringField(required=True)
    value = mongoengine.StringField(required=False)
    trend = mongoengine.StringField(required=False)
    comments = mongoengine.StringField(required=False)
    original_string = mongoengine.StringField(required=False)

class Prioritas(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    value = mongoengine.StringField(required=True)
    level = mongoengine.StringField(required=False)
    comments = mongoengine.StringField(required=False)

class Celkituzes(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    objektum = mongoengine.StringField(required=True)
    problema = mongoengine.StringField(required=True)
    celkituzes = mongoengine.StringField(required=False)
    eszkozok = mongoengine.StringField(required=False)
    hatarido = mongoengine.StringField(required=False)
    erintettek = mongoengine.StringField(required=False)
    megjegyzes = mongoengine.StringField(required=False)

class TeruletiCelkituzes(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    sentence = mongoengine.StringField(required=True)

class Elohely(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    elohely = mongoengine.StringField(required=True)
    kiterjedes =mongoengine.EmbeddedDocumentField(Kiterjedes, required=True)
    reprezentativitas = mongoengine.EmbeddedDocumentField(Populacio, required=True)
    trend = mongoengine.StringField(required=True)
    kotodo_fajok = mongoengine.ListField()
    termeszetvedelmi_helyzet = mongoengine.EmbeddedDocumentListField(TermeszetvedelmiHelyzet, required=True)
    prioritas = mongoengine.EmbeddedDocumentField(Prioritas,required=True)
    celkituzesek = mongoengine.EmbeddedDocumentListField(Celkituzes)

    meta = {
            'db_alias': 'core',
            'collection': 'elohelyek'
    }

class Faj(mongoengine.EmbeddedDocument):
    oid = mongoengine.ObjectIdField(required=True, default=ObjectId, primary_key=True)
    faj = mongoengine.StringField(required=True)
    allomany_tipus = mongoengine.StringField(required=False)
    jelolonek_javasolt = mongoengine.BooleanField(default=True)
    allomanynagysag = mongoengine.EmbeddedDocumentField(Allomanynagysag, required=True)
    populacio = mongoengine.EmbeddedDocumentField(Populacio, required=True)
    trend = mongoengine.StringField(required=True)
    kotodo_elohelyek = mongoengine.ListField()
    termeszetvedelmi_helyzet = mongoengine.EmbeddedDocumentListField(TermeszetvedelmiHelyzet, required=True)
    prioritas = mongoengine.EmbeddedDocumentField(Prioritas,required=True)
    celkituzesek = mongoengine.EmbeddedDocumentListField(Celkituzes)

    meta = {
        'db_alias': 'core',
        'collection': 'fajok'
    }

class Site(mongoengine.Document):
    terulet_neve = mongoengine.StringField(required=True)
    terulet_tipusa = mongoengine.StringField(required=True)
    terulet_azonositoja = mongoengine.StringField(required=True)
    terulet_kiterjedese = mongoengine.FloatField()
    updated_at = mongoengine.DateTimeField()
    created_at = mongoengine.DateTimeField()


    meta = {
        'db_alias': 'core',
        'collection': 'sites'
    }

class SiteDocument(mongoengine.Document):
    site_id = mongoengine.StringField(required=True)
    datum = mongoengine.StringField()
    jelolo_elohelyek = mongoengine.EmbeddedDocumentListField(Elohely)
    jelolo_fajok = mongoengine.EmbeddedDocumentListField(Faj)
    teruleti_celkituzesek = mongoengine.ListField(mongoengine.StringField())
    keszultseg = mongoengine.DictField()
    elerhetoseg = mongoengine.URLField()

    meta = {
        'db_alias': 'core',
        'collection': 'site_documents'
    }