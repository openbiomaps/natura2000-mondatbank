import mammoth
import mammoth.transforms
import re
import bs4
from unidecode import unidecode
from mondatbank.fejezet import *

def transform_paragraph(element):

    if is_heading(element):
        return element.copy(style_id="Heading2")
    else:
        return element



def is_heading(element):
    """Checks if a paragraph of the parsed document is intended to be a heading."""
    
    element.children = mammoth.transforms.get_descendants_of_type(element, mammoth.documents.Run)

    return (
                element.children and
                len(element.children) and
                isinstance(element.children[0], mammoth.documents.Run) and 
                (
                    element.children[0].font_size == 16.0 or
                    element.style_id == 'Heading1' or
                    element.style_id == 'Cmsor1' or
                    element.style_id == 'JAzonostadatok'
                )
            )


def slugify(string, keep_non_alpha = False):
    if (isinstance(string, str)):
        string = re.sub(r"\s+", "_", unidecode(string).lower().strip())
        if (keep_non_alpha):
            return string
        else:
            return re.sub(r'\W+', '', string)
    else:
        return string

def mkinst(cls, *args, **kwargs):
    try:
        return globals()[cls](*args, **kwargs)
    except:
        raise NameError("Class %s is not defined" % cls)

def remove_markup(h):
    """Flattens the strings from a BS object"""
    strr = ''
    for i in h.children:
        if isinstance(i, bs4.element.NavigableString):
            strr = strr + str(i.string)
        else:
            strr = strr + remove_markup(i)
    return strr   

def string_to_float(string):
    number = re.findall(r'\d+[,\.]?\d*', string)
    if number:
        extracted_number = float(number[0].replace(',', '.'))
        return extracted_number
    else:
        return False

def find_date_before_h2(soup):
    
    # Az első h2 elem megtalálása
    first_h2 = soup.find('h2')
    
    # Minden p elem kigyűjtése az első h2 elem előtt
    if first_h2:
        p_elements = first_h2.find_all_previous('p')
    else:
        p_elements = soup.find_all('p')
    
    # Dátum formátum: évszám, magyar hónap neve, nap (pl. '2023. október 15.')
    date_pattern = re.compile(r'\d{4}\. (?:[\wáéőúűóüöÁÉŐÚŰÓÜÖ]+) \d{1,2}\.', re.IGNORECASE)
    
    # Keresünk olyan p elemet, amiben dátum található
    for p in p_elements:
        print(p.get_text())
        if date_pattern.search(p.get_text()):
            return p.get_text()
    
    # Ha nincs találat, visszaadunk None-t
    return None