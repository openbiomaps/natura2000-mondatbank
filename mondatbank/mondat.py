from bs4 import Tag
import mondatbank.helpers as hlp
import json_fix, json, uuid

class Mondat():
    def __init__(self, tag):
        self.tag = tag
        self.name = tag.name
        self.txt = tag.get_text()
        self.key = hlp.slugify(self.tag.title) if self.tag.title else str(uuid.uuid4())
        self.value = []

    def __json__(self):
        return self.value

    def __str__(self):
        return self.key + ': ' + json.dumps(self.value, indent=2)

class Egysoros(Mondat):

    def __init__(self, tag):
        super().__init__(tag)
        spl = self.txt.split(':', 1)
        self.key = hlp.slugify(spl[0].strip())
        self.value = spl[1].strip()#{'id': 0, 'value': spl[1].strip()})

class Lista(Mondat):

    def __init__(self, tag):
        self.i = 0
        super().__init__(tag)

        for i, l in enumerate(self.tag.contents):
            if (l.string):
                self.value.append(l.string)#{'id': i, 'value': l.string})
            elif (l.find('ul')):
                subul = l.find('ul').extract()
                subul.title = l.string
                self.value.append({'value': l.string, 'children': Lista(subul)})
            else:
                self.value.append(l.get_text())#{'id': i, 'value': l.get_text()})
    
    def __iter__(self):
        return self

    def __next__(self):
        if self.i < len(self.value):
            self.i += 1
            return self.value[self.i-1]
        else:
            raise StopIteration

class Tablazat(Mondat):

    header = []

    def __init__(self, tag):
        super().__init__(tag)

        for i, row in enumerate(self.tag.find_all('tr')):
            d = {}
            cells = row.find_all('td')
            if (len(cells) == 0):
                cells = row.find_all('th')
            for j, cell in enumerate(cells):
                if (i == 0):
                    self.header.append(hlp.slugify(cell.get_text()))
                else:
                    d[self.header[j]] = cell.get_text()
            if (i > 0):
                self.value.append(d)
