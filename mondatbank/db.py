import mongoengine
from dotenv import load_dotenv
import os


def global_init():
    load_dotenv()
    mongoengine.register_connection(
            alias = 'core',
            name = os.getenv('DB_NAME'),
            host = os.getenv('DB_HOST'),
            port = int(os.getenv('DB_PORT')),
            username = os.getenv('DB_USER'),
            password = os.getenv('DB_PASS')
            )

