## Installation

### Download the source code
```
git clone git@gitlab.com:openbiomaps/natura2000-mondatbank.git
cd natura2000-mondatbank
```

### Creating and activating a virtual environment

```
virtualenv venv
source venv/bin/activate
```

### Install mondatbank

```
pip install .
```
- use --editable flag if you want to do some development
```
pip install --editable .
```

### Setting up database connection

```
cp env_sample .env
```
- update DB_NAME, DB_HOST, ... params

## Usage

```
mondatbank parse [-v] [-h] [--save-html] [--save-json] path/to/the/document.docx
```

### optional arguments:
-  -v                    verbose output
-  -h, --help            show this help message and exit
-  --save-html           save the doc as a html file
-  --save-json           save the parsed document as a JSON file in tmp/output.json

### Example file

```
mondatbank parse -v doc/minta.docx
```
